$(function () {
  /* - - - - - - */
  //$("button").click(hide);

  $("button").click(() => {
    $("ul").hide(1000);
  });

  $("#btn2").click(() => {
    $("ul").show();
  });

  /* - - - - - - */
});

// jQuery methods go here...

//
//
//
//$("#hide").click(function(){
//  $("p").hide();
//});
//
//$("#show").click(function(){
//  $("p").show();
//});
//$(this).hide() - hides the current element.
//
//$("p").hide() - hides all <p> elements.
//
//$(".test").hide() - hides all elements with class="test".
//
//$("#test").hide() - hides the element with id="test".
